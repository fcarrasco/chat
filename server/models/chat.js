// chat model
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Chat = new Schema({
  name: String,
  description: String
});

module.exports = mongoose.model('chats', Chat);
