// message model
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Message = new Schema({
  chat_id: String,
  username: String,
  message: String,
  created: { type: Date, default: Date.now }
});

module.exports = mongoose.model('messages', Message);
