#!/usr/bin/env node

var debug = require('debug')('passport-mongo');
var app = require('./app');

var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

app.set('port', process.env.PORT || 8080);

io.sockets.on('connection', function(socket) {
    socket.on('newMessage', function(message) {
        io.sockets.emit('broadcastNewMessage', message);
    });
});

server.listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
