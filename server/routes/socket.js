module.exports = function (socket) {
    socket.on('newMessage', function(message) {
        io.sockets.emit('broadcastNewMessage', message);
    });
};
