var express = require('express');
var router = express.Router();

var Chat = require('../models/chat.js');
var Message = require('../models/message.js');

router.post('/new', function(req, res) {
    var chat = new Chat({ name: req.body.name, description: req.body.description });
    chat.save (function(err, chat) {
        if (err) {
          return res.status(500).json({
            err: err
          });
        }
        return res.status(200).json({status: 'Chat created'});
    });
});

router.get('/list', function(req, res) {
    Chat.find({}, function(err, chats) {
        if (err) {
            return res.status(500).json({
                err: err
            });
        }
        return res.status(200).json(chats);
    });
});

router.get('/:id/messages', function(req, res) {
    var id = req.params.id;
    Message.find({chat_id: id}, function(err, messages) {
        if (err) {
            return res.status(500).json({
                err: err
            });
        }
        return res.status(200).json(messages);
    }).sort({created: 1});
});

router.post('/:id/message', function(req, res) {
    var id = req.params.id;
    var message = new Message ({
        chat_id: id,
        username: req.user.username,
        message: req.body.message
    });

    message.save (function(err, message) {
        if (err) {
          return res.status(500).json({
            err: err
          });
        }
        return res.status(200).json({status: 'Message created'});
    });
});

module.exports = router;
