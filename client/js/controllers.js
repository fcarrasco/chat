angular.module('chat')
.controller('headerController',
  ['$scope', '$location', 'AuthService',
  function ($scope, $location, AuthService) {
        $scope.AuthService = AuthService;
        $scope.$watch('AuthService.getUsername()', function(username) {
            $scope.username = username;
        })
        $scope.$watch('AuthService.isLoggedIn()', function(logged) {
            $scope.isLoggedIn = logged;
        });
}])
.controller('loginController',
  ['$scope', '$location', 'AuthService',
  function ($scope, $location, AuthService) {

    $scope.login = function () {

      $scope.error = false;
      $scope.disabled = true;

      AuthService.login($scope.loginForm.username, $scope.loginForm.password)
        .then(function () {
          $location.path('/');
          $scope.disabled = false;
          $scope.loginForm = {};
        })
        .catch(function () {
          $scope.error = true;
          $scope.errorMessage = "Invalid username and/or password";
          $scope.disabled = false;
          $scope.loginForm = {};
        });

    };

}])
.controller('logoutController',
  ['$scope', '$location', 'AuthService',
  function ($scope, $location, AuthService) {

    $scope.logout = function () {

      AuthService.logout()
        .then(function () {
          $location.path('/login');
        });

    };

}])
.controller('registerController',
  ['$scope', '$location', 'AuthService',
  function ($scope, $location, AuthService) {

    $scope.register = function () {

      $scope.error = false;
      $scope.disabled = true;

      AuthService.register($scope.registerForm.username, $scope.registerForm.password)
        .then(function () {
          $location.path('/login');
          $scope.disabled = false;
          $scope.registerForm = {};
        })
        .catch(function () {
          $scope.error = true;
          $scope.errorMessage = "Something went wrong!";
          $scope.disabled = false;
          $scope.registerForm = {};
        });

    };

}])
.controller('chatsController',
  ['$scope', '$location', 'ChatService',
  function ($scope, $location, ChatService) {
      ChatService.getList(function(data) {
          $scope.chats = data;
      }, function(err) {
          console.log(err);
      });
}])
.controller('newChatController',
  ['$scope', '$location', 'ChatService',
  function ($scope, $location, ChatService) {

    $scope.save = function () {

      $scope.error = false;
      $scope.disabled = true;

      ChatService.save($scope.chatForm.name, $scope.chatForm.description)
        .then(function () {
          $location.path('/');
          $scope.disabled = false;
          $scope.chatForm = {};
        })
        .catch(function () {
          $scope.error = true;
          $scope.errorMessage = "Something went wrong!";
          $scope.disabled = false;
          $scope.chatForm = {};
        });

    };
}])
.controller('chatController',
  ['$scope', '$location', '$route', 'mySocket', 'ChatService',
  function ($scope, $location, $route, mySocket, ChatService) {
      ChatService.getMessages($route.current.params.id, function(data) {
          $scope.messages = data;
      }, function(err) {
          console.log(err);
      });
      mySocket.on('broadcastNewMessage', function(data) {
          console.log(data);
          if(data.chat_id == $route.current.params.id) {
              $scope.messages.push(data);
          }
      });
}])
.controller('newMessageController',
  ['$scope', '$location', '$route', 'mySocket', 'ChatService', 'AuthService',
  function ($scope, $location, $route, mySocket, ChatService, AuthService) {
    $scope.newMessage = function () {

      $scope.error = false;
      $scope.disabled = true;

      ChatService.newMessage($route.current.params.id, $scope.messageForm.message)
        .then(function () {
          $scope.disabled = false;
          mySocket.emit('newMessage', {
              message: $scope.messageForm.message,
              chat_id: $route.current.params.id,
              username: AuthService.getUsername()
          });
          $scope.messageForm = {};
        })
        .catch(function () {
          $scope.error = true;
          $scope.errorMessage = "Something went wrong!";
          $scope.disabled = false;
          $scope.messageForm = {};
        });

    };
}]);
