angular.module('chat').factory('ChatService',
    ['$q', '$timeout', '$http',
        function ($q, $timeout, $http) {
            return ({
                getList: getList,
                getMessages: getMessages,
                newMessage: newMessage,
                save: save
            });

            function getList(success, error) {
                $http.get('/chat/list')
                    .success(success)
                    .error(error);
            }

            function getMessages(id, success, error) {
                $http.get('/chat/' + id + '/messages/')
                    .success(success)
                    .error(error);
            }

            function save (name, description) {
                var deferred = $q.defer();
                $http.post('/chat/new',
                {name: name, description: description})
                .success(function (data, status) {
                    if(status === 200 && data.status){
                        deferred.resolve();
                    } else {
                        deferred.reject();
                    }
                })
                .error(function (data) {
                    deferred.reject();
                });
                return deferred.promise;
            }

            function newMessage (chat_id, message) {
                var deferred = $q.defer();
                $http.post('/chat/' + chat_id + '/message',
                {message: message})
                .success(function (data, status) {
                    if(status === 200 && data.status){
                        deferred.resolve();
                    } else {
                        deferred.reject();
                    }
                })
                .error(function (data) {
                    deferred.reject();
                });
                return deferred.promise;
            }
        }
    ]
);
