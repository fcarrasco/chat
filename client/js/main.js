var chat = angular.module('chat', ['ngRoute', 'btford.socket-io', 'luegg.directives']);

chat.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'partials/chat/list.html',
      controller: 'chatsController',
      access: {restricted: true}
    })
    .when('/chat/new', {
      templateUrl: 'partials/chat/new.html',
      controller: 'newChatController',
      access: {restricted: true}
    })
    .when('/chat/:id', {
      templateUrl: 'partials/chat/show.html',
      controller: 'chatController',
      access: {restricted: true}
    })
    .when('/chat/:id/message', {
      controller: 'newMessageController',
      access: {restricted: true}
    })
    .when('/login', {
      templateUrl: 'partials/user/login.html',
      controller: 'loginController'
    })
    .when('/logout', {
      controller: 'logoutController'
    })
    .when('/register', {
      templateUrl: 'partials/user/register.html',
      controller: 'registerController'
    })
    .otherwise({
      redirectTo: '/'
    });
});

chat.run(function ($rootScope, $location, $route, AuthService) {
  $rootScope.$on('$routeChangeStart',
    function (event, next, current) {
      AuthService.getUserStatus()
      .then(function(){
        if (next && next.access && next.access.restricted && !AuthService.isLoggedIn()){
          $location.path('/login');
          $route.reload();
        }
      });
  });
});

chat.factory('mySocket', function (socketFactory) {
  return socketFactory();
})
